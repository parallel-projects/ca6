#include "pch.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include <Windows.h>


#define N  14

int solutions;

int put(int Queens[], int row, int column)
{
	int i;
	for (i = 0; i < row; i++) {
		int tempQueen = Queens[i];
		if (tempQueen == column || abs(tempQueen - column) == (row - i))
			return -1;
	}
	
	Queens[row] = column;

	if (row == N - 1) {
		#pragma omp atomic
			solutions++;
	}
	else {
		
		for (i = 0; i < N; i++) { // increment row
			put(Queens, row + 1, i);
		}
		
	}
	return 0;
}


void solve(int Queens[]) {
	int i;
	#pragma omp parallel private(i) num_threads(8)
	{
		#pragma omp for schedule(dynamic) nowait
			for (i = 0; i < N; i++) {
				put(new int[N], 0, i);
			}

	}
}



int main()
{
	printf("Student IDs:\n810196518\n810196562\n\n");

	int Queens[N];
	LARGE_INTEGER start, end;
	LARGE_INTEGER frequency;
	double elapsedTime;

	QueryPerformanceCounter(&start);

	solve(Queens);

	QueryPerformanceCounter(&end);

	QueryPerformanceFrequency(&frequency);
	elapsedTime = (double)(end.QuadPart - start.QuadPart) / (double) frequency.QuadPart;

	printf("# solutions %d time: %lf \n", solutions, elapsedTime);

	return 0;

}